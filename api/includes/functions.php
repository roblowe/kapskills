<?php
function _o($code, $text, $payload = false) {
	exit(json_encode(array('code' => $code, 'text' => $text, 'payload' => $payload)));
}

function get_the_user($var = false) {
	session_start('ks');
	if (!empty($_SESSION)) {
		if (!$var) {
			return $_SESSION;
		} else {
			return $_SESSION[$var];
		}
	} else {
		return false;
	}
}

function is_exception($object, $method) {
	$exceptions = array(
		'user' => array('add'),
		'sets' => array('get_skills', 'get_original_teams', 'get_project_status')
	);

	if (isset($exceptions[$object]) && in_array($method, $exceptions[$object])) {
		return true;
	} else {
		return false;
	}
}

function explode_skills($skills) {
	$array_skills = explode(",", $skills);
	return json_encode($array_skills);
}
?>