# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.40-0ubuntu0.12.04.1)
# Database: kapskills
# Generation Time: 2014-11-20 13:48:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE kapskills;


# Dump of table people
# ------------------------------------------------------------

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `profile_picture` text,
  `original_team` varchar(255) DEFAULT NULL,
  `manager` tinyint(1) DEFAULT NULL,
  `notes` text,
  `current_project` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `people` WRITE;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;

INSERT INTO `people` (`id`, `name`, `email`, `profile_picture`, `original_team`, `manager`, `notes`, `current_project`)
VALUES
	(1,'Fabio Salimbeni','fabio.salimbeni@kaplan.com','fabio.jpg','Wordpress',0,'I\'ve followed some courses online about Objective-C',1),
	(2,'Rob Lowe','rob.lowe@kaplan.com','rob.jpg','Wordpress',1,'I\'m the scrum master',1),
	(3,'Mauro Franchi','mauro.franchi@kaplan.com','mauro.jpg','Enterprise',0,'I\'m awesome and I eat chocolate every night',2),
	(4,'Gunay Tacel','gunay.tacel@kaplan.com','gunay.jpg','QA Team',0,'I\'m awesome',NULL);

/*!40000 ALTER TABLE `people` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `name`)
VALUES
	(1,'KapSkills'),
	(2,'Travel Calendar App');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skills
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skills`;

CREATE TABLE `skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;

INSERT INTO `skills` (`id`, `name`)
VALUES
	(1,'Wordpress'),
	(2,'Bootstrap 3'),
	(3,'jQuery'),
	(4,'QA'),
	(5,'Everything');

/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skills_people
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skills_people`;

CREATE TABLE `skills_people` (
  `id_people` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_skills` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_people`,`id_skills`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skills_people` WRITE;
/*!40000 ALTER TABLE `skills_people` DISABLE KEYS */;

INSERT INTO `skills_people` (`id_people`, `id_skills`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(2,1),
	(2,2),
	(2,3),
	(3,5),
	(4,4);

/*!40000 ALTER TABLE `skills_people` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
