<?php

/**
 * Debug
 */
print_r($_GET);

/**
 * Include core functions.
 */

include 'includes/functions.php';

/**
 * Autoload all classes.
 */

spl_autoload_register(function ($class) {
    include 'classes/' . $class . '.class.php';
});

/**
 * Initialise classes.
 */
$user = new User();
$project = new Project();
$sets = new Sets();

/**
 * First make sure that an object and method has been passed through
 */

if (isset($_GET['object']) && isset($_GET['method'])) {
	/**
	 * Check is authentication is needed to continue to run the method
	 */
	
	/*
	if (!is_exception($_GET['object'], $_GET['method']) && !$user->is_logged_in()) {
		_o(0, 'You must be authenticated to access this object and method');
	}
	*/
	/**
	 * Map the object and method to an appropriate call.
	 */
	switch ($_GET['object']) {
		case "user":
			$object = $user;
			break;
		case "project":
			$object = $project;
			break;
		case "sets":
			$object = $sets;
			break;
	}

	/**
	 * Call the appropriate method.
	 */
	call_user_func(array($object, $_GET['method']));
} else {
	_o(0, 'You must pass an object and method.');
}
?>