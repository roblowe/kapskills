<?php
class Sets extends DB
{
	public function get_skills($echo = true) {
		$skills = $this->select('ks_skill');

		if ($echo) {
			_o(1, '', $skills);
		} else {
			return $skills;
		}
	}

	public function get_original_teams($echo = true) {
		$original_teams = $this->select('ks_original_teams');

		if ($echo) {
			_o(1, '', $original_teams);
		} else {
			return $original_teams;
		}
	}

	public function get_project_status($echo = true) {
		$project_status = $this->select('ks_project_status');

		if ($echo) {
			_o(1, '', $project_status);
		} else {
			return $project_status;
		}
	}

}
?>