<?php
class DB
{
	private $sql;

	/**
	 * This is called whenever any queries to the DB are made.
	 * @return <boolean>
	 */
	public function __construct() {
		$this->sql = new mysqli('localhost', 'root', 'root', 'ks_local');

		if ($this->sql->connect_error) {
			$this->sql = false;
		}
	}

	/**
	 * Abstraction layer for mysql_query.
	 */
	
	public function query($query) {
		return $this->sql->query($query);
	}

	/**
	 * Abstraction layer to check if a connection does exist.
	 * @return [type] [description]
	 */
	public function does_exist() {
		return ($this->sql != false);
	}

	/**
	 * Escape value passed through and return it.
	 */
	public function escape($string) {
		return $this->sql->real_escape_string($string);
	}

	/**
	 * array_map_insert
	 *
	 * Takes an array of data, and converts it to valid a MySQL insert statement.
	 */
	
	public function array_map_insert($array) {
		$fields = "(";
		$values = " VALUES (";

		foreach ($array as $f => $v) {
			$fields .= $this->escape($f) . ",";
			$values .= "'" . $this->escape($v) . "',";
		}

		return rtrim($fields, ",") . ")" . rtrim($values, ",") . ")";
	}

	/**
	 * array_map_update
	 *
	 * Takes an array of data, as well as a WHERE condition and converts it to a valid MySQL update statement.
	 */
	
	public function array_map_update($array) {
		$fields_and_values = "";
		foreach ($array as $f => $v) {
			$fields_and_values .= $this->escape($f) . "='" . $this->escape($v) . "',";
		}

		return rtrim($fields_and_values, ",");
	}

	/**
	 * Insert
	 *
	 * Abstraction method for inserting data into DB.
	 */
	
	public function insert($table, $array) {
		$query = "INSERT INTO $table " . $this->array_map_insert($array);
		return ($this->query($query));
	}

	/**
	 * Update
	 *
	 * Abstraction method for updating data in the DB.
	 */
	
	public function update($table, $array, $where) {
		$query = "UPDATE $table SET " . $this->array_map_update($array) . " WHERE " . $this->escape($where);
		return ($this->query($query));
	}

	/**
	 * Delete
	 *
	 * Abstraction method for deleting data in the DB.
	 */
	
	public function delete($table, $where) {
		$query = "DELETE FROM $table WHERE " . $this->escape($where);
		return ($this->query($query));
	}

	/**
	 * Select
	 * 
	 * Abstrtaction method for selecting data from the database.
	 */
	public function select($table, $array = false) {
		$query = "SELECT ";
		if (is_array($array)) {
			$query .= (isset($array['fields'])) ? $array['fields'] : "*";
			$query .= (isset($array['where'])) ? " FROM $table WHERE " . $array['where'] : "";
			$query .= (isset($array['limit'])) ? " LIMIT " . $array['limit'] : "";
			$query .= (isset($array['offset'])) ? " OFFSET " . $array['offset'] : "";
			$query .= (isset($array['order'])) ? " ORDER BY " . $array['order'] : "";
		} else {
			$query .= " * FROM $table";
		}
		$result = $this->query($query);
		$rows   = array();

		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}

		if (count($rows) > 0) {
			return $rows;
		} else {
			return false;
		}
	}
}
?>