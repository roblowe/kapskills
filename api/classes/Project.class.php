<?php
class Project extends DB
{
	/**
	 * Add a new project
	 */
	public function add() {
		if ($this->is_manager()) {
			// Extract/format POST data
			$new_project = array(
				'project_name' => $_POST['project_name'],
				'project_status' => $_POST['project_status'],
				'project_skills' => explode_skills($_POST['project_skills']),
				'project_owner' => $_POST['project_owner'],
				'project_description' => $_POST['project_description']
			);
			if($this->insert('ks_project', $new_project)) {
				_o(1, 'Successfully created a new project');
			} else {
				_o(0, 'Project creation failed, please try again');
			}
		} else {
			_o(0, 'You do not have the required permissions to access this method.');
		}
	}

	/**
	 * Edit an existing project.
	 * @return [type] [description]
	 */
	public function edit() {
		if ($this->is_manager() && $this->owns_project($_POST['project_owner'])) {
			$upd_project = array();
			foreach ($_POST as $f => $v) {
				if ($f = 'project_skills') {
					$v = explode_skills($v);
				}
				$upd_project[$f] = $v;
			}
			if ($this->update('ks_projects', $upd_project, 'project_id = ' . $_POST['id'])) {
				_o(1, 'Successfully updated existing project');
			} else {
				_o(0, 'Project cannot be updated, please try again');
			}
		} else {
			_o(0, 'You do not have sufficient permissions to edit/delete this project.');
		}
	}

	/**
	 * Delete an existing project
	 * @return [type] [description]
	 */
	public function del() {
		if ($this->is_manager() && $this->owns_project($_POST['project_owner'])) {
			if ($this->delete('ks_project', 'id = ' . $_POST['id'])) {
				_o(1, 'Successfully deleted project');
			} else {
				_o(0, 'Project cannot be deleted, please try again.');
			}
		} else {
			_o(0, 'You do not have sufficient permissions to edit/delete this project.');
		}
	}

	/**
	 * [get description]
	 * @return [type] [description]
	 */
	public function get($echo = true, $where = false) {
		// Quick dirty way to to ID based lookups
		if (isset($_GET['id'])) {
			$where = array('id', $_GET['id']);
		}

		if (!$where) {
			$projects = $this->select('ks_project');
		} else {
			$projects = $this->select('ks_project', array('where' => $where[0] . " = '" . $where[1] . "'"));
		}

		// Strip slashes before outputting the project description
		foreach ($projects as &$project) {
			$project['project_description'] = stripslashes($project['project_description']);
		} 

		if ($echo) {
			_o(1, '', $projects);
		} else {
			return $projects;
		}
	}

	/**
	 * Check if user is manager
	 */
	private function is_manager() {
		return (get_the_user('is_manager'));
	}

	private function owns_project($user_id) {
		return ($user_id == get_the_user('id'));
	}
}
?>