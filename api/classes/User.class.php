<?php
class User extends DB
{

	/**
	 * Register account.
	 * @return [type] [description]
	 */
	public function add() {
		$new_user = array(
			'firstname' => $_POST['firstname'],
			'lastname' => $_POST['lastname'],
			'email' => $_POST['email'],
			'password' => md5($_POST['password']),
			'skills' => explode_skills($_POST['skills']),
			'original_team' => $_POST['original_team'],
			'is_manager' => $_POST['is_manager'],
			'profile_image' => $_POST['profile_image'],
			'skills_future' => explode_skills($_POST['skills_future']),
		);

		if($this->insert('ks_person', $new_user)) {
			_o(1, 'User successfully registered');
		} else {
			_o(0, 'User cannot be registered, please try again');
		}
	}

	/**
	 * Edit account.
	 */
	public function edit() {
		$upd_user = array();
		foreach ($_POST as $f => $v) {
			if ($f == 'password') {
				$v = md5($v);
			} else if ($f == 'skills' || $f == 'skills_future'){
				$v = explode_skills($v);
			}
			$upd_user[$f] = $v;
		}

		if ($this->update('ks_person', $upd_user, 'id = ' . get_the_user('id'))) {
			_o(1, 'User successfully updated');
		} else {
			_o(0, 'User cannot be updated, please try again.');
		}
	}

	/**
	 * Delete account.
	 */
	public function del() {
		if ($this->delete('ks_person', 'id = ' . get_the_user('id'))) {
			_o(1, 'User successfully deleted');
		} else {
			_o(0, 'User cannot be deleted, please try again.');
		}
	}

	/**
	 * Login to account.
	 * @return [type] [description]
	 */
	public function login() {
		$password = md5($_POST['password']);
		$user = $this->get(false, array('email', $_POST['email']));
		if ($password == $user['password']) {
			$this->set_session($user);
			_o(1, 'Logging you in');
		} else {
			_o(0, 'Incorrect login information.');
		}
	}

	/**
	 * Reset password
	 */
	public function reset_password_form() {

	}

	/**
	 * Email reset link.
	 */
	public function reset_password_email() {

	}

	/**
	 * Check if logged in.
	 */
	public function is_logged_in() {
		session_start();
		return (!empty($_SESSION));
	}

	/**
	 * Logout.
	 */
	
	public function logout() {
		$this->del_session();
	}

	/**
	 * Get user profile.
	 */
	
	public function get($echo = true, $where = false) {
		if (!$where) {
			$users = $this->select('ks_person');
		} else {
			$users = $this->select('ks_person', array('where' => $where[0] . " = '" . $where[1] . "'"));
			if (count($users) == 1) {
				$users = $users[0];
			}
		}

		if ($echo) {
			_o(1, '', $users);
		} else {
			return $users;
		}
	}

	/**
	 * Del session.
	 */
	
	public function del_session() {
		$_SESSION = array();
		session_destroy();
	}

	/**
	 * Set session.
	 */
	
	public function set_session($session_array) {
		session_set_cookie_params(86400, '/', '.kapskills.dev');
		session_start();
		$_SESSION = $session_array;
	}
}
?>