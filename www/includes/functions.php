<?php

function the_original_team() {
	$teams = array('dotnet' => '.Net', 'wp' => 'Wordpress', 'devops' => 'DevOps', 'drupal' => 'Drupal');

	return $teams;
}


function get_the_projects() {

	$projects = array(
		array(
		'name' => 'Travel Calendar App',
		'code' => 'JRMTRAVCAL',
		'description' => 'SharePoint Travel Calendar App to track intern business trips',
		'technology' => 'ASP.NET, SharePoint, AngularJS',
		'manager' => 'Jigger Moran', 
		'people' =>  'Mauro Franchi, Riccardo DiNuzzo'
		),
		array(
		'name' => 'Brivo Class Intergration',
		'code' => 'DMBRIVO',
		'description' => 'Automatic update of student images to Brivo',
		'technology' => 'MULE',
		'manager' => 'Dario Griffo',
		'people' =>  'Dario Griffo, Manuel Guerrero'
		),
		array(
		'name' => 'KapSkills',
		'code' => 'YRFKS',
		'description' => 'Managment tool to create and mange project and project team with a focus on people skill and aims',
		'technology' => 'AngularJS, Bootststrap 3, JSON API',
		'manager' => 'Rob Lowe',
		'people' =>  'Rob Lowe, Yousef Soliman, Fabio Salimbeni'
		)
	);


	return $projects;



}

/**
 * Retrieves skills for a given user.
 *
 * @param $id_people Id for a user.
 *
 * @return array with the names of the skills this user has assigned,
 * NULL if it has none.
 */
function get_people_skills($id_people) {
  $handle = kaplan_skills_db_connect();

  $query = "SELECT s.name
    FROM skills_people sp
    LEFT JOIN skills s on sp.id_skills = s.id
    WHERE id_people = ?";

  $params = array($id_people);

  $stmt = $handle->prepare($query);
  $stmt->execute($params);

  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row['name'];
  }

  // Return null if there are no results,
  // so we can implode() the result of this function safely.
  return !empty($results) ? $results : NULL;
}

/**
 * Retrieves users from db, optionally filtering by name.
 *
 * @param string $search An optional search string for filtering names.
 *
 * @return array Nested array containing data for all the wanted users.
 *
 * @todo: Provide an order criteria.
 */
function get_the_people($search = '') {
  $handle = kaplan_skills_db_connect();

  $query = "SELECT pe.id, pe.name, pe.email, pe.profile_picture,
    pe.original_team, pe.manager, pe.notes, pr.name AS current_project
    FROM people pe
    LEFT JOIN projects pr ON pe.current_project = pr.id";
  $params = array();

  // We'll only filter if a $search string has been provided.
  // Otherwise, we'll return all the users.
  if (!empty($search)) {
    $search = strtolower($search);
    $query .= " WHERE name LIKE :search";
    $params = array(
      ':search' => "%$search%",
    );
  }

  $stmt = $handle->prepare($query);
  $stmt->execute($params);

  $results = array();

  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
     // Add skills if the user has any.
    if ($skills = get_people_skills($row['id'])) {
      $row['skills'] = implode(', ', $skills);
    }
    $results[] = $row;
  }

  return $results;
}

/**
 * Provides a PDO database connection.
 *
 * @return PDO
 */
function kaplan_skills_db_connect() {
  $db = array(
    'database' => 'kapskills',
    'username' => 'root',
    'password' => 'Kaplan123',
  );

  // Establishing connection.
  $handle = new PDO("mysql:host=localhost;dbname={$db['database']}", $db['username'], $db['password']);
  return $handle;
}

/**
 * Add users to db
 *
 * @param string $search An optional search string for filtering names.
 *
 * @return array Nested array containing data for all the wanted users.
 *
 * @todo: Provide an order criteria.
 */

function kaplan_add_users($args) {
    $handle = kaplan_skills_db_connect();

    $query = "INSERT INTO people (id,name,email,profile_picture,original_team,manager,notes,current_project) VALUES (:id,:name,:email,:profile_picture,:original_team,:manager,:notes,:current_project)";

    $stmt = $handle->prepare($query);

    $id = rand(10,1000);

    $var_id = $stmt->bindParam(':id', $id);
    $stmt->bindParam(':name', $args['name']);
    $stmt->bindParam(':email', $args['email']);
    $stmt->bindParam(':profile_picture', $args['profile_picture']);
    $stmt->bindParam(':original_team', $args['original_team']);
    $stmt->bindParam(':manager', $args['manager']);
    $stmt->bindParam(':notes', $args['notes']);
    $stmt->bindParam(':current_project', $args['current_project']);

    $results = $stmt->execute();

    return $results;

}


function set_new_data($data) {

    $args = array();

    foreach($data as $single) {

        $keyvalue = explode('=',$single);

        $key = $keyvalue[0];
        $value = $keyvalue[1];


        $args[$key] = $value;


    }

    return $args;

}


function kaplan_add_projects($args) {
    $handle = kaplan_skills_db_connect();

    $query = "INSERT INTO projects (id,name) VALUES (:id,:name)";

    $stmt = $handle->prepare($query);

    $id = rand(10,1000);

    $var_id = $stmt->bindParam(':id', $id);
    $stmt->bindParam(':name', $args['name']);

    $results = $stmt->execute();

    return $results;

}