<?php 
	include './includes/functions.php';



	if($_GET['manager'] == 'yes' && $_GET['manager_name'] == 'Jigger Moran') {

		$manager = true;
		$manager_name =  $_GET['manager_name'];

	}
	else {

		$manager = false; 
	}
?>
<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
<link rel="stylesheet" href="./assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="./assets/css/app.css">
<link rel="stylesheet" href="./assets/css/bootstrap-theme.min.css">




<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>
<script src="./assets/js/libs/bootstrap.min.js"></script>
<script src="./assets/js/libs/typeahead.js"></script>
<script src="./assets/js/libs/bootstrap-tagsinput.js"></script>
<script src="./assets/js/app.js"></script>
<script src="./assets/js/ngDraggable.js"></script>
</head>

<body ng-app="kapSkills">
<div class="container">
	<div class="row">

		<div class="header col-md-12">
		<h1>KapSkills</h1>
		<h3>Kaplan International Project Management tool</h3>
		</div>

	</div>
	<div class="content row" ng-controller="ProjectController">
	
	

		<div class="col-md-12">

		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="#projects" role="tab" data-toggle="tab">Projects</a></li>
		  <li><a href="#people" role="tab" data-toggle="tab">People</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="projects" >
		  
		  	
		  <div class="container">
		  	<div class="row">
		  		<div class="col-md-12">
		  	  		<form ng-submit="filterProjects()"  >
		  	  			Search text <input ng-change="filterProject()" ng-model="keyword"/>
		  	  			<button>Filter</button>
		  	  		</form>
		  		</div>
		  		
		  		
		  	</div>
		  	<div class="row">
		  	

	  		<div ng-repeat="project in projects">
                <div class="col-md-6">
	  			<div id="{{project.code}}">
		  		<div class="wborder">
		  	
			  		<div class="row detail">
			  			<div class="col-md-12">
			  				<h4>{{project.name}}</h4><?php //echo ($manager && $manager_name == $project['manager']) ? '<button class="btn btn-info">Edit project</button>' : '' ?>
			  			</div>
			  		</div>
			  		<div class="row detail">
			  			<div class="col-md-3">
			  				<p>Code: </p>
			  			</div>
			  			<div class="col-md-9">
			  				<p>{{project.code}}</p>
			  			</div>
			  		</div>
	
			  		<div class="row detail">
			  			<div class="col-md-3">
			  				<p>Technology: </p>
			  			</div>
			  			<div class="col-md-9">
			  				<p>{{project.technology}}</p>
			  			</div>
			  		</div>
			  		<div class="row detail">
			  			<div class="col-md-3">
			  				<p>Description: </p>
			  			</div>
			  			<div class="col-md-9">
			  				<p>{{project.description}}</p>
			  			</div>
			  		</div>
			  		<div class="row detail">
			  			<div class="col-md-3">
			  				<p>Manager: </p>
			  			</div>
			  			<div class="col-md-9">
			  				<p>{{project.manager}}</p>
			  			</div>
			  		</div>
			  		<div class="row detail last">
			  			<div class="col-md-3">
			  				<p>Team: </p>
			  			</div>
			  			<div class="col-md-9">
			  				<p>{{project.people}}</p>
			  			</div>
			  		</div>
		  		</div>
		  		</div>	
		  		</div>
		  	</div>
		  		</div>
		  	</div>
		  </div>
		  <div class="tab-pane" id="people">
		  	<div class="container">
		  	<div class="row">
		  		<div class="col-md-12">
		  	  		<form  >
		  	  			Search text <input ng-change="filterUsers()" ng-model="keyword_user"/>
		  	  			
		  	  		</form>
		  		</div>
		  		
		  		
		  	</div>
		  	<div class="row">
		  		
		 
		  

		  		<div ng-repeat="user in users" class="col-md-6" ng-hide="user.hide">
                    <div >
		  			<div id="user{{user.id}}" ng-drag="true" ng-drag-data="user" >
			  		<div class="wborder">
			  		<div class="row">
			  			<div class="col-md-3">
				  			<div class="row detail last">
				  			<div class="col-md-3">
				  				<img src="assets/img/{{user.profile_picture}}" class="img profile">
				  			</div>
				  		</div>
				  		</div>
				  		<div class="col-md-8 ">
				  		<div class="row detail">
				  			<div class="col-md-12">
				  				<h4>{{user.name}}</h4>
				  			</div>
				  		</div>
				  		<div class="row detail">
				  			<div class="col-md-3">
				  				<p>Email: </p>
				  			</div>
				  			<div class="col-md-9">
				  				<p>{{user.email}}</p>
				  			</div>
				  		</div>
	
				  		<div class="row detail">
				  			<div class="col-md-3">
				  				<p>Team: </p>
				  			</div>
				  			<div class="col-md-9">
				  				<p>{{user.original_team}}</p>
				  			</div>
				  		</div>
				  		<div class="row detail">
				  			<div class="col-md-3">
				  				<p>Skills: </p>
				  			</div>
				  			<div class="col-md-9">
				  				<p>{{user.skills}}</p>
				  			</div>
				  		</div>
				  		<div ng-if="user.manager == 'yes'">
				  			<div class="row detail">
					  			<div class="col-md-3">
					  				<p>Manager</p>
					  			</div>
					  		</div>
				  		
				  		</div>
				  		<div class="row detail">
				  			<div class="col-md-3">
				  				<p>Current Project:</p>
				  			</div>
				  		<div ng-if="user.current_project != ''">
				  		
				  			<div class="col-md-9">
				  				<p>{{user.current_project}}</p>
				  			</div>
				  		</div>
				  		<div ng-if="user.current_project == ''">
				  			<div class="col-md-9 red">
				  				<p>Project free</p>
				  			</div> 
				  		</div>
				  		</div>
				  		<div class="row detail last">
				  			<div class="col-md-3">
				  				<p>Notes: </p>
				  			</div>
				  			<div class="col-md-9">
				  				<p>{{user.notes}}</p>
				  			</div>
				  		</div>
				  		</div>
				  	</div>
			  		</div>	
			  		</div>
		  		</div>
		  		</div>
		  			
		  		</div>
		  	</div>
		  </div>
		  
		</div>
		<div class="row">				
		<div class="wborder">
			<div class="col-md-12">
			Project Name: Example Project
			</div>
			<div class="col-md-12 user-area" ng-drop="true" ng-drop-success="onDropComplete($data, $event)"  ng-drag-data="user" ng-model="droplist">
				Please drag users here
				<div ng-repeat="user in users" ng-show="user.hide">
					<div class="col-md-3">
						<a  ng-click="removeDropUser(user)" >Remove</a>
						<img src="assets/img/{{user.profile_picture}}" class="img profile">
						<h3>{{user.name}}</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		</div>
	</div>
	
</div>

</body>
</html>