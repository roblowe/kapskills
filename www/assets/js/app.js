jQuery(document).ready(function() {
	
	jQuery('.login-button').click(function(e) {
		e.preventDefault();

		jQuery('.login').removeClass('hidden');

		if (!jQuery('.register').hasClass('hidden')) {

			jQuery('.register').addClass('hidden');

		}
		
		return false;

	});

	jQuery('.register-button').click(function(e) {
		e.preventDefault();

		jQuery('.register').removeClass('hidden');

		if (!jQuery('.login').hasClass('hidden')) {

			jQuery('.login').addClass('hidden');

		}
		
		return false;

	});



	var skillSet = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: {
	    url: 'assets/js/libs/skillSet.json',
	    filter: function(list) {
	      return jQuery.map(list, function(skill) {
	        return { name: skill }; });
	    }
	  }
	});


	skillSet.initialize();

	jQuery('.inputSkill text').tagsinput({
	  typeaheadjs: {
	    name: 'skills',
	    displayKey: 'name',
	    valueKey: 'name',
	    source: skillSet.ttAdapter()
	  }});

	console.log(jQuery('.inputSkill text').tagsinput('items'));


	

});

var kapSkills = angular.module('kapSkills',['ngDraggable']);

kapSkills.controller('ProjectController', ['$scope', function($scope) {




 
  
  var users = [
  
  	{ 
  		'id' : '456',
  		'name' : 'Fabio Salimbeni',
		'email' : 'fabio.salimbeni@kaplan.com',
		'profile_picture' : 'fabio.jpg',
		'original_team' : 'Wordpress',
		'manager' : 'no', 
		'skills' : 'Wordpress, Bootstrap 3, jQuery',
		'notes' : 'I\'ve followed some courses online about Objective-C',
		'current_project' : 'KapSkills'
	},
	{
		'id' : '457',
		'name' : 'Rob Lowe',
		'email' : 'rob.lowe@kaplan.com',
		'profile_picture' : 'rob.jpg',
		'original_team' : 'Wordpress',
		'manager' : 'yes', 
		'skills' :  'Wordpress, Bootstrap 3, jQuery',
		'notes' : 'I\m the scrum master',
		'current_project' : 'KapSkills'
	},
	{
		'id' : '458',
		'name' : 'Mauro Franchi',
		'email' : 'mauro.franchi@kaplan.com',
		'profile_picture' : 'mauro.jpg',
		'original_team' : 'Enterprise',
		'manager' : 'no', 
		'skills' :  'Everything',
		'notes' : 'I\'m awesome and I eat chocolate every night',
		'current_project' : 'Travel Calendar App'
	},	
	{
		'id' : '459',
		'name' : 'Guany Tacel',
		'email' : 'gunay.tacel@kaplan.com',
		'profile_picture' : 'gunay.jpg',
		'original_team' : 'QA Team',
		'manager' : 'no', 
		'skills' :  'QA',
		'notes' : 'I\'m awesome',
		'current_project' : ''
	}
];

$scope.users = users;

var projects = [
  
  	{ 
		'name' : 'Travel Calendar App',
		'code' : 'JRMTRAVCAL',
		'description' : 'SharePoint Travel Calendar App to track intern business trips',
		'technology' : 'ASP.NET, SharePoint, AngularJS',
		'manager' : 'Jigger Moran', 
		'people' :  'Mauro Franchi, Riccardo DiNuzzo'
	},
	{
		'name' : 'Brivo Class Intergration',
		'code' : 'DMBRIVO',
		'description' : 'Automatic update of student images to Brivo',
		'technology' : 'MULE',
		'manager' : 'Dario Griffo',
		'people' :  'Dario Griffo, Manuel Guerrero'
	},
	{
		'name' : 'KapSkills',
		'code' : 'YRFKS',
		'description' : 'Managment tool to create and mange project and project team with a focus on people skill and aims',
		'technology' : 'AngularJS, Bootststrap 3, JSON API',
		'manager' : 'Rob Lowe',
		'people' :  'Rob Lowe, Yousef Soliman, Fabio Salimbeni'
	}
];

var dropusers = [];

$scope.projects = projects;
$scope.dropusers = dropusers;

  $scope.filterProject = function() {

  
  angular.forEach(projects, function (project) {
  
  
	  if( project.name.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1 ||  project.code.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1 ||  project.description.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1 ||  project.technology.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1 ||  project.manager.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1 || project.people.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1) {
	   	
	  		$('#'+project.code).show();
	  
	   }
	   else{
		   $('#'+project.code).hide();
	   }
	  
  });
   
  	
  };
	  
$scope.filterUsers = function() {

  
  angular.forEach(users, function (user) {
  
  
	  if( user.name.toLowerCase().indexOf($scope.keyword_user.toLowerCase()) > -1 ||  user.email.toLowerCase().indexOf($scope.keyword_user.toLowerCase()) > -1 ||  user.original_team.toLowerCase().indexOf($scope.keyword_user.toLowerCase()) > -1 ||  user.skills.toLowerCase().indexOf($scope.keyword_user.toLowerCase()) > -1) {
	   	
	  		$('#user'+user.id).show();
	  
	   }
	   else{
		   $('#user'+user.id).hide();
	   }
	  
  });
   
  	
  };
	  
	  
  $scope.onDropComplete=function(user,evt){
  	$scope.dropusers.push(user);
  	user.hide = 1;
  	// $('#user'+user.id).parent().hide();
  };
  
   $scope.removeDropUser=function(user){
	   user.hide = 0;
  };
	  
  
}]);







