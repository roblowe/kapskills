<?php 
	include './includes/functions.php';

    if($_POST['registered'] == 'yes') {

        $postdata = explode("&",file_get_contents('php://input'));


        if(kaplan_add_users(set_new_user($postdata))) {

            $registration_completed = true;

        }

    }
?>
<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
<link rel="stylesheet" href="./assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="./assets/css/app.css">
<link rel="stylesheet" href="./assets/css/bootstrap-theme.min.css">




<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>
<script src="./assets/js/libs/bootstrap.min.js"></script>
<script src="./assets/js/libs/typeahead.js"></script>
<script src="./assets/js/libs/bootstrap-tagsinput.js"></script>
<script src="./assets/js/app.js"></script>
</head>

<body ng-app="kapSkills">
<div class="container">
	<div class="row">

		<div class="header col-md-12">
		<h1>KapSkills</h1>
		<h3>Kaplan International Project Management tool</h3>
		</div>

	</div>
	<div class="content row">
		<div class="col-md-6">
		 	<button type="button" class="btn btn-primary right login-button">Login</button>
		</div>
		<div class="col-md-6">
		 	<button type="button" class="btn btn-info left register-button">Register</button>
		</div>
	</div>
    <?php
        if ($registration_completed){
           ?>
            <div class="content row">
                <div class="col-md-12">
                    <h1>Registration complete! <a href="/summpary.php">Click here to continue</a></h1>
                </div>
            </div>
           <?php
        }
    ?>
	<div class="content row hidden login">
		<div class="col-md-6 col-md-offset-3">
		<form role="form" action="summary.php">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		</form>
		</div>
	</div>

	<div class="content row hidden register">
		<div class="col-md-6 col-md-offset-3">
		<form role="form" action="index.php" method="post">
		  <div class="form-group">
		    <label for="InputName1">Name</label>
		    <input type="text" class="form-control" id="InputName1" placeholder="Name" required name="name">
		  </div>
		  <div class="form-group">
		    <label for="InputEmail1">Email address</label>
		    <input type="email" class="form-control" id="InputEmail1" placeholder="Enter email" required name="email">
		  </div>
		  <div class="form-group">
		    <label for="InputPassword1">Password</label>
		    <input type="password" class="form-control" id="InputPassword1" placeholder="Password" required name="password">
		  </div>
		  <div class="form-group">
		    <label for="ProfilePicture">Profile picture</label>
		    <input type="file" id="ProfilePicture" name="profile_picture">
		    <p class="help-block">Insert your profile picture here.</p>
		  </div>
		   <div class="form-group">
		    <label for="original_team">Original Team</label>
		     <select name="original_team" class="form-control" required>
		    <?php $teams = the_original_team();



		    	foreach ($teams as $key => $team) {
		    		echo '<option value="'.$key.'">'.$team.'</option>';
		    	}

		    ?>
		    </select>
		  </div>
		  <div class="form-group inputSkill">
		    <label for="inputSkills">Skills:</label>
		    <input type="text" class="form-control" id="inputSkills" data-role="tagsinput" required name="inputSkills">
		  </div>
		  <div class="checkbox">
		    <label>
		      <input type="checkbox"> Tick if you are a Project Maanger
		    </label>
		  </div>
		  <div class="form-group inputSkillToImptrove">
		    <label for="inputSkillsToImprove">Skills to improve:</label>
		    <input type="text" class="form-control" id="inputSkillsToImprove" data-role="tagsinput" required name="inputSkillstoImprove">
		  </div>
		  <div class="form-group">
		  	<label for="txtNotes">Notes:</label>
		  	<textarea class="form-control" rows="3" id="txtNotes" required name="notes"></textarea>
		  </div>
          <input type="hidden" name="registered" value="yes">
		  <button type="submit" class="btn btn-default">Register</button>
		</form>
		</div>
	</div>

</div>

</body>
</html>